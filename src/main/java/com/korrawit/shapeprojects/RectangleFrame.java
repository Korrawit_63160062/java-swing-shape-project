/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeprojects;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JOptionPane;

/**
 *
 * @author DELL
 */
public class RectangleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Rectangle");
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblWidth = new JLabel("width : ", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setBackground(Color.white);
        lblWidth.setLocation(5, 5);
        lblWidth.setOpaque(true);
        frame.add(lblWidth);

        final JTextField txtWidth = new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        frame.add(txtWidth);
        
        JLabel lblHeight = new JLabel("height : ", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setBackground(Color.white);
        lblHeight.setLocation(5, 35);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);

        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 35);
        frame.add(txtHeight);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 35);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Rectangle width = ??? height = ??? area =  ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.blue);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    String strHeight = txtHeight.getText();
                    double width = Double.parseDouble(strWidth);
                    double height = Double.parseDouble(strHeight);
                    Rectangle rectangle = new Rectangle(width, height);
                    lblResult.setText("Rectangle width = " + String.format("%.2f", rectangle.getWidth()) + " height = " + String.format("%.2f", rectangle.getHeight()) + " area = " + String.format("%.2f", rectangle.calArea()) + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtHeight.setText("");
                    txtWidth.requestFocus();
                }
            }
        }
        );

        frame.setVisible(true);

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.shapeprojects;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JOptionPane;

/**
 *
 * @author DELL
 */
public class TriangleFrame {

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Triangle ( equilateral )");
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblBase = new JLabel("base : ", JLabel.TRAILING);
        lblBase.setSize(50, 20);
        lblBase.setBackground(Color.white);
        lblBase.setLocation(5, 5);
        lblBase.setOpaque(true);
        frame.add(lblBase);

        final JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(60, 5);
        frame.add(txtBase);

        JLabel lblHeight = new JLabel("height : ", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setBackground(Color.white);
        lblHeight.setLocation(5, 35);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);

        final JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(60, 35);
        frame.add(txtHeight);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 35);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Triangle base = ??? height = ??? area =  ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 80);
        lblResult.setBackground(Color.blue);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    String strHeight = txtHeight.getText();
                    double base = Double.parseDouble(strBase);
                    double height = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(base, height);
                    lblResult.setText("Triangle base = " + String.format("%.2f", triangle.getBase()) + " height = " + String.format("%.2f", triangle.getHeight()) + " area = " + String.format("%.2f", triangle.calArea()) + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error : Please input number", "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtBase.requestFocus();
                }
            }
        }
        );

        frame.setVisible(true);

    }

}
